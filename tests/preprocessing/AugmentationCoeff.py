import unittest
from src.preprocessing.AugmentationCoeff import AugmentationCoeff
import numpy as np

width = 500
height = 300
log = None

class AugmentationCoeffUnitTest(unittest.TestCase):
    def test_default_cstr(self):
        coeff = AugmentationCoeff({}, width, height)
        self.assertTrue(coeff.is_default_value('spatial'))
        self.assertTrue(coeff.is_default_value('chromatic'))
        self.assertTrue(coeff.is_default_value('chromatic-eigen'))
        self.assertTrue(coeff.is_default_value('effect'))

    def test_generate_coeff(self):
        param = {
          'rand_type': 'uniform',
          'exp': False,
          'mean': 0,
          'spread': 0.2,
          'prob': 0.5,
        }
        coeff = AugmentationCoeff({}, width, height)

        # 'uniform'
        value = coeff._AugmentationCoeff__generate_coeff(param)
        self.assertGreaterEqual(value, -0.2)
        self.assertLessEqual(value, 0.2)

        # 'gaussian'
        param['rand_type'] = 'gaussian'
        value = coeff._AugmentationCoeff__generate_coeff(param)
        self.assertGreaterEqual(value, -0.6)
        self.assertLessEqual(value, 0.6)

        # 'bernoulli'
        param['rand_type'] = 'bernoulli'
        value = coeff._AugmentationCoeff__generate_coeff(param)
        self.assertTrue(value == 1 or value == 0)

        # 'uniform_bernoulli'
        param['rand_type'] = 'uniform_bernoulli'
        param['prob'] = 0.00001
        value = coeff._AugmentationCoeff__generate_coeff(param)
        self.assertEqual(value, 0.0)

        param['prob'] = 0.999
        value = coeff._AugmentationCoeff__generate_coeff(param)
        self.assertGreaterEqual(value, -0.2)
        self.assertLessEqual(value, 0.2)

        # 'gaussian_bernoulli'
        param['rand_type'] = 'gaussian_bernoulli'
        param['prob'] = 0.00001
        value = coeff._AugmentationCoeff__generate_coeff(param)
        self.assertEqual(value, 0.0)

        param['prob'] = 0.999
        value = coeff._AugmentationCoeff__generate_coeff(param)
        self.assertGreaterEqual(value, -0.6)
        self.assertLessEqual(value, 0.6)

    def test_spatial_cstr(self):
        params = {
            'translate': {
              'rand_type': "uniform_bernoulli",
              'exp': False,
              'mean': 0,
              'spread': 0.4,
              'prob': 1.0,
            },
            'rotate': {
              'rand_type': "uniform_bernoulli",
              'exp': False,
              'mean': 0,
              'spread': 0.4,
              'prob': 1.0,
            },
            'zoom': {
              'rand_type': "uniform_bernoulli",
              'exp': True,
              'mean': 0.2,
              'spread': 0.4,
              'prob': 1.0,
            },
            'squeeze': {
              'rand_type': "uniform_bernoulli",
              'exp': True,
              'mean': 0,
              'spread': 0.3,
              'prob': 1.0,
            },
        }
        coeff = AugmentationCoeff(params, width, height)
        self.assertNotEqual(coeff.dx, 1.0) # check that it isn't still the default
        self.assertGreaterEqual(coeff.dx, -0.4)
        self.assertLessEqual(coeff.dx, 0.4)
        self.assertNotEqual(coeff.dy, 1.0) # check that it isn't still the default
        self.assertGreaterEqual(coeff.dy, -0.4)
        self.assertLessEqual(coeff.dy, 0.4)
        self.assertNotEqual(coeff.angle, 1.0) # check that it isn't still the default
        self.assertGreaterEqual(coeff.angle, -0.4)
        self.assertLessEqual(coeff.angle, 0.4)
        self.assertNotEqual(coeff.zoom_x, 1.0) # check that it isn't still the default
        self.assertGreaterEqual(coeff.zoom_x, 0.8)
        self.assertLessEqual(coeff.zoom_x, 1.9)
        self.assertNotEqual(coeff.zoom_y, 1.0) # check that it isn't still the default
        self.assertGreaterEqual(coeff.zoom_y, 0.8)
        self.assertLessEqual(coeff.zoom_y, 1.9)

    # TODO: Test all the other methods

if __name__ == '__main__':
    unittest.main()
