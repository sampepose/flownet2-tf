from src.preprocessing.preprocess import preprocess, flow_difference
from src.preprocessing.AugmentationCoeff import AugmentationCoeff
import tensorflow as tf
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

width = 512
height = 384

params = {
    'image_a': {
        'scale': True,
        'crop_width': 448,
        'crop_height': 320,
        'translate': {
          'rand_type': "uniform_bernoulli",
          'exp': False,
          'mean': 0,
          'spread': 0.4,
          'prob': 1.0,
        },
        'rotate': {
          'rand_type': "uniform_bernoulli",
          'exp': False,
          'mean': 0,
          'spread': 0.4,
          'prob': 1.0,
        },
        'zoom': {
          'rand_type': "uniform_bernoulli",
          'exp': True,
          'mean': 0.2,
          'spread': 0.4,
          'prob': 1.0,
        },
        'squeeze': {
          'rand_type': "uniform_bernoulli",
          'exp': True,
          'mean': 0,
          'spread': 0.3,
          'prob': 1.0,
        },
        'noise': {
          'rand_type': "uniform_bernoulli",
          'exp': False,
          'mean': 0.03,
          'spread': 0.03,
          'prob': 1.0,
        },
    },
    # All preprocessing to image A will be applied to image B in addition to the following.
    # Note: If you overwrite a spatial transform (translate/rotate/zoom/squeeze), only the new spatial transform will be applied.
    'image_b': {
        'scale': True,
        'crop_width': 448,
        'crop_height': 320,
        'translate': {
          'rand_type': "gaussian_bernoulli",
          'exp': False,
          'mean': 0,
          'spread': 0.03,
          'prob': 1.0,
        },
        'rotate': {
          'rand_type': "gaussian_bernoulli",
          'exp': False,
          'mean': 0,
          'spread': 0.03,
          'prob': 1.0,
        },
        'zoom': {
          'rand_type': "gaussian_bernoulli",
          'exp': True,
          'mean': 0,
          'spread': 0.03,
          'prob': 1.0,
        },
        'gamma': {
          'rand_type': "gaussian_bernoulli",
          'exp': True,
          'mean': 0,
          'spread': 0.02,
          'prob': 1.0,
        },
        'brightness': {
          'rand_type': "gaussian_bernoulli",
          'exp': False,
          'mean': 0,
          'spread': 0.02,
          'prob': 1.0,
        },
        'contrast': {
          'rand_type': "gaussian_bernoulli",
          'exp': True,
          'mean': 0,
          'spread': 0.02,
          'prob': 1.0,
        },
        'color': {
          'rand_type': "gaussian_bernoulli",
          'exp': True,
          'mean': 0,
          'spread': 0.02,
          'prob': 1.0,
        },
    }
}

def readFlow(name):
    if name.endswith('.pfm') or name.endswith('.PFM'):
        return readPFM(name)[0][:,:,0:2]

    f = open(name, 'rb')

    header = f.read(4)
    if header.decode("utf-8") != 'PIEH':
        raise Exception('Flow file header does not contain PIEH')

    width = np.fromfile(f, np.int32, 1).squeeze()
    height = np.fromfile(f, np.int32, 1).squeeze()

    flow = np.fromfile(f, np.float32, width * height * 2).reshape((height, width, 2))

    return flow.astype(np.float32)

def writeFlow(name, flow):
    f = open(name, 'wb')
    f.write('PIEH'.encode('utf-8'))
    np.array([flow.shape[1], flow.shape[0]], dtype=np.int32).tofile(f)
    flow = flow.astype(np.float32)
    flow.tofile(f)
    f.flush()
    f.close()

import os
print os.getpid()
raw_input("Press Enter to continue...")

sess = tf.Session()

image_a = tf.reshape(tf.constant(mpimg.imread('./0000002-img0.png')), [1, height, width, 3])
image_b = tf.reshape(tf.constant(mpimg.imread('./0000002-img1.png')), [1, height, width, 3])
flow = tf.reshape(tf.constant(readFlow('./0000002-gt.flo')), [1, height, width, 2])

params_a = params['image_a']
params_a['scale'] = params['scale']
params_a['crop_height'] = params['crop_height']
params_a['crop_width'] = params['crop_width']
image_a_processed, coeff_a = preprocess(image_a, params_a)
image_a_processed = sess.run(image_a_processed)

params_b = params['image_b']
params_b['scale'] = params['scale']
params_b['crop_height'] = params['crop_height']
params_b['crop_width'] = params['crop_width']
image_b_processed, coeff_b = preprocess(image_b, params_b, coeff_a)
image_b_processed = sess.run(image_b_processed)

flow_processed = flow_difference(flow, coeff_a, coeff_b, [params_a['crop_height'], params_a['crop_width']])
flow_processed = sess.run(flow_processed)

plt.figure(2)
plt.imshow(image_a_processed[0, :, :, :])
plt.savefig('./o/preprocess/img0.png')
plt.figure(3)
plt.imshow(image_b_processed[0, :, :, :])
plt.savefig('./o/preprocess/img1.png')
plt.show()


writeFlow('./o/preprocess/preprocess.flo', flow_processed[0, :, :, :])

# tf_img = tf.reshape(tf.constant(image), [1, height, width, 3])
# tf_imgs = tf.concat([tf_img, tf_img, tf_img, tf_img, tf_img], 0)
# outImages = preprocess(tf_imgs, params)
# outImages = tf.Session().run(outImages)
# print outImages.shape
#
# for i in range(outImages.shape[0]):
#     plt.figure(i)
#     plt.imshow(outImages[i, :, :, :])
# plt.show()
