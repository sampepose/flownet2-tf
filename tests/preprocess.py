import unittest
from src.preprocessing import preprocess

width = 500
height = 300

class PreprocessUnitTest(unittest.TestCase):
    def test_compute_spatial_trans_matrix(self):
        params = {
            'rotate': {
              'rand_type': "uniform_bernoulli",
              'exp': False,
              'mean': 0,
              'spread': 0.4,
              'prob': 1.0,
            }
        }
        coeff = preprocess.AugmentationCoeff(params, width, height)
        print coeff
        print preprocess.compute_spatial_trans_matrix(coeff)

if __name__ == '__main__':
    unittest.main()
